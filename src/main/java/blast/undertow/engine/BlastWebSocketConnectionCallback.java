/*
 * Grownup Software Limited.
 */
package blast.undertow.engine;

import static blast.BlastConstants.ORIGIN_HEADER_KEY;
import blast.client.WebSocketRequestDetailsImpl;
import blast.server.BlastServer;
import blast.undertow.client.UndertowServerClient;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author dhudson - Mar 24, 2017 - 2:08:18 PM
 */
public class BlastWebSocketConnectionCallback implements WebSocketConnectionCallback {

    private final BlastServer blast;

    public BlastWebSocketConnectionCallback(BlastServer server) {
        blast = server;
    }

    @Override
    public void onConnect(WebSocketHttpExchange wshe, WebSocketChannel wsc) {
        WebSocketRequestDetailsImpl details = new WebSocketRequestDetailsImpl();
        details.setHeaders(wshe.getRequestHeaders());
        details.setParams(wshe.getRequestParameters());
        details.setOrigin(wshe.getRequestHeader(ORIGIN_HEADER_KEY));
        details.setQueryString(wshe.getQueryString());

        try {
            URI uri = new URI(wshe.getRequestURI());
            details.setRequestPath(uri.getPath());
            details.setRequestURI(uri);
        } catch (URISyntaxException ignore) {
        }

        details.setRemoteAddress(wsc.getDestinationAddress().getHostName());
        details.resolveRemoteAddress();

        UndertowServerClient client = new UndertowServerClient(blast, wsc, details);
        client.postClientConnectingEvent();
        wsc.resumeReceives();
    }

}
