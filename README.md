![Alt text](http://www.gusl.co/blast/resources/images/b-circle-trans-100.png) *on*   ![Alt text](./resources/images/undertow_logo-stacked_200px.png)

Built using Undertow version 1.2.11.Final

# Gradle
```groovy
repositories { maven { url "http://dl.bintray.com/gusl/blast" } }

compile 'co.gusl:blast-engine-undertow:1.0.0'
```

# Embedded


Simply add a new route to the router as follows.

```java
        Undertow.builder()
                .addHttpListener(properties.getEndpoint().getPort(), host)
                .setHandler(path().addPrefixPath(BlastConstants.BLAST_ROUTE_PATH, 
                        websocket(new BlastWebSocketConnectionCallback(blastServer)))).build();
```


# Stand alone

To run the Standalone version of blast use ..

```java
     BlastServer blast = Blast.blast(new UndertowEngine())
```
This will create a Undertow HTTP Server and configure using the properties from Blast Properties.
As the `UndertowEngine` is a `Controllable` Blast will start and stop the engine.

## Gradle Runner

`./gradlew undertow:undertow-engine:run`
