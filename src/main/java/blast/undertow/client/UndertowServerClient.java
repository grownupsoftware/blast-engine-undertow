/*
 * Grownup Software Limited.
 */
package blast.undertow.client;

import blast.client.AbstractBlastServerClient;
import blast.client.ClosingReason;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import java.io.IOException;
import blast.client.WebSocketRequestDetails;
import blast.server.BlastServer;

/**
 *
 * @author dhudson - Mar 24, 2017 - 2:10:23 PM
 */
public class UndertowServerClient extends AbstractBlastServerClient {

    private final WebSocketChannel webSocketChannel;
    private final String clientID;

    public UndertowServerClient(BlastServer server, WebSocketChannel wsc, WebSocketRequestDetails details) {
        super(server, details);
        webSocketChannel = wsc;
        clientID = createShortClientID();
        webSocketChannel.getReceiveSetter().set(new UndertowWebSocketListener(this));
    }

    @Override
    public void close(ClosingReason reason) {
        try {
            webSocketChannel.close();
        } catch (IOException ignore) {
        }
    }

    @Override
    public String getClientID() {
        return clientID;
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        WebSockets.sendTextBlocking(new String(bytes), webSocketChannel);
    }

    @Override
    public void queueMessage(byte[] bytes) {
        postQueuedMessageEvent(bytes);
    }

}
